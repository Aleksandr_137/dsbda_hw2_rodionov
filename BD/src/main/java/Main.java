import java.sql.*;
import java.util.Random;

import com.mysql.jdbc.Driver;

class Main {
    private static final String DATABASE_URL = "jdbc:mysql://localhost:3306"; //"jdbc:mysql://207.180.210.6:3306";
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String USER = "root"; //rodionov
    private static final String PASSWORD = "#master"; //PmvRPkwa
    private static final String MY_DB = "hw2_test";
    private static final String MY_TABLE = "metrics";



    public static Statement statement = null;

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Connection connection = null;
        String SQL;

        try
        {
            Class.forName(JDBC_DRIVER);

            connection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
            statement = connection.createStatement();

            DatabaseMetaData dbm = connection.getMetaData();
            ResultSet databases = dbm.getCatalogs();

            while (databases.next()) {
                String databaseName = databases.getString(1);
                if (databaseName.equals(MY_DB)) {
                    SQL = "DROP DATABASE " + MY_DB;
                    statement.executeUpdate(SQL);
                    break;
                }
            }

            SQL = "CREATE DATABASE " + MY_DB;
            statement.executeUpdate(SQL);

            connection.setCatalog(MY_DB);
            statement = connection.createStatement();

            SQL = "CREATE TABLE " + MY_TABLE +
                  "(id INTEGER not NULL AUTO_INCREMENT, " +
                  " gr_id INTEGER, " +
                  " time_ DOUBLE, " +
                  " val_metric INTEGER, " +
                  " PRIMARY KEY (id))";
            statement.execute( SQL ) ;

            Random rnd = new Random(System.currentTimeMillis());

            int min = 1;
            int max = 5;
            int gr_id, val_metric;
            double time = 1510670916247.0;
            double delta = 200/1000.0;

            for(int i = 0; i < 10; ++i) {
                gr_id = min + rnd.nextInt(max - min + 1);
                val_metric = gr_id * 100 + (-gr_id * 10) + rnd.nextInt((gr_id * 10) - (-gr_id * 10) + 1);

                add(MY_TABLE, gr_id, time, val_metric);
                time += delta;
            }

            out("*", MY_TABLE);

            System.out.println("Database successfully created...");
        }
        finally {
            if(statement!=null){
                statement.close();
            }
            if(connection!=null){
                connection.close();
            }
        }
    }

    static void out(String var, String table) throws SQLException {
        String SQL = "SELECT " + var +" FROM " + table;
        ResultSet rs = statement.executeQuery(SQL);

        while(rs.next()){
            int id  = rs.getInt("id");
            int gr_id  = rs.getInt("gr_id");
            double time  = rs.getDouble("time_");
            int val_metric  = rs.getInt("val_metric");

            System.out.printf("ID: %d, gr_id: %d, time: %f, val_metr: %d.\n", id, gr_id, time, val_metric);
        }
        rs.close();
    }

    static void add(String table, int gr_id, double time, int val_metric) throws SQLException {
        String SQL = "INSERT INTO " + table + "(gr_id, time_, val_metric) " +
                "VALUES(" + gr_id + ", " + time + ", " + val_metric + ")";
        statement.execute( SQL ) ;
    }
}