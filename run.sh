#!/bin/bash

echo "--- Job started ---"

# Spark Version 2 is needed
export SPARK_MAJOR_VERSION=2

cd spark
spark-submit --master local[1] --class ru.mephi.dsbda.Main target/spark-1.0-SNAPSHOT.jar hdfs://sandbox-hdp.hortonworks.com:8020/hw2/input/part-* hdfs://sandbox-hdp.hortonworks.com:8020/hw2/output 140
cd ..

echo "--- Job Done ---"

echo "- Getting result -"

hadoop fs -text /hw2/output/* > ./result.txt

echo "--- Result ---"

cat ./result.txt
