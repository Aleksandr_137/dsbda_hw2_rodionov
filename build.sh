#!/bin/bash

# Spark Version 2 is needed
export SPARK_MAJOR_VERSION=2

cd spark
mvn package
cd ../

echo "===================Package BD================================"

cd BD
mvn package
cd ../