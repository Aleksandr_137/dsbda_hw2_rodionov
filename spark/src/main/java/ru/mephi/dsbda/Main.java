package ru.mephi.dsbda;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

/**
 * Main.java
 * Main class with spark job implementation
 */
public class Main {

    /**
     * Entry point.
     * @param args cmd line arguments.
     * @throws Exception print message and exit.
     */
    public static void main(String[] args) throws Exception {

        // Check arguments.
        if (args.length != 3) {
			System.out.println("You must enter three parameters: Input data, output data, scale 'sec'.");
            return;
        }
		
        Integer scale = Integer.parseInt(args[2]);

        /** Create Spark Session */
        SparkSession spark = SparkSession.builder().appName("dbsa_hw2_application").getOrCreate();

        /** Template input table */
        StructType _table = new StructType(new StructField[]{
                new StructField("id",    DataTypes.IntegerType,      false, Metadata.empty()),
                new StructField("gr_id", DataTypes.IntegerType,      false, Metadata.empty()),
                new StructField("time_", DataTypes.DoubleType,       false, Metadata.empty()),
                new StructField("val_metric", DataTypes.IntegerType, false, Metadata.empty())
        });

        /** Read input data */
        Dataset<Row> data_frame = spark.read().schema(_table).option("header", "false").option("delimiter", ",").csv(args[0]);

        /** Create temp table */
        data_frame.createOrReplaceTempView("temp_table");

       // Dataset<Row> tt = spark.sql("SELECT gr_id, CAST(time_ AS LONG) as time_, val_metric FROM temp_table");
       // tt.show();

		//                 CAST(<выражение> AS <тип данных>)
		// SELECT gr_id, CAST((time_ / 120) AS INTEGER) * 120 as time_, val_metric FROM temp_table
		
		// SELECT gr_id, time_, '120s' as scale, avg(val_metric) AS avg_val FROM temp_table2 GROUP BY gr_id, time_ ORDER BY gr_id, time_
		
        /** Business logic */
        Dataset<Row> data_frame1 = spark.sql("SELECT gr_id, CAST((time_ / " + scale.toString() + ") AS LONG) * "
					+ scale.toString() + " as time_, val_metric FROM temp_table");

        data_frame1.createOrReplaceTempView("temp_table2");
		
        Dataset<Row> data_frame2 = spark.sql("SELECT gr_id, time_, '" + scale.toString() +
                    "s' as scale, avg(val_metric) AS avg_val_metric FROM temp_table2 GROUP BY gr_id, time_ ORDER BY gr_id, time_");

        /** Overwrite final data in file */
        data_frame2.write().mode(SaveMode.Overwrite).option("header", "false").option("delimiter", ",").csv(args[1]);
    }
}