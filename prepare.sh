#!/bin/bash

echo "Start prepare."

# Spark Version 2 is needed
export SPARK_MAJOR_VERSION=2

# Create dir (HDFS)
echo "Create dir (HDFS)"
hadoop fs -mkdir /hw2/output

# Clean up dst folder (HDFS)
echo "Cleaning up destination"
hadoop fs -rm -r /hw2/input 
hadoop fs -rm -r /hw2/output/* 

# Generate input data in MySQL
echo "Data generating and import to MySQL..."
java -jar ./BD/target/spark-1.0-SNAPSHOT.jar

echo "Exporting data from MySQL in hdfs"
# Move data to HDFS
sqoop import --connect jdbc:mysql://207.180.210.6:3306/hw2_test --username rodionov --password PmvRPkwa --table metrics --target-dir /hw2/input/

echo "Finish prepare."
